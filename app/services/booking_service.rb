class BookingService
  include ActiveModel::Validations

  delegate :rand_free_table, to: :@restaurant

  INVALID_PARAMS = Class.new(StandardError)
  PAST_TIME      = Class.new(StandardError)
  BAD_TIME_RANGE = Class.new(StandardError)
  ALREADY_BOOKED = Class.new(StandardError)
  HE_IS_DRUNK    = Class.new(StandardError)
  CLOSED_TIME    = Class.new(StandardError)

  attr_accessor :restaurant_id, :table_id, :user_id, :from, :to
  attr_accessor :future, :half_hour, :already_booked, :he_is_drunk

  with_options presence: true do
    validates :restaurant_id, :user_id, :table_id, :from, :to
  end

  validates :future, in_future: true
  validates :half_hour, half_hour: true
  validates :already_booked, already_booked: true
  validates :he_is_drunk, he_is_drunk: true

  def initialize(params = {})
    params.each do |key, value|
      self.instance_variable_set("@#{key}".to_sym, value)
    end
  end

  def self.book(&block)
    booking = new(yield)

    raise_error(booking.errors) unless booking.valid?

    Reservation.create!(
      restaurant_id: booking.restaurant_id,
      user_id: booking.user_id,
      table_id: booking.table_id,
      from: booking.from,
      to: booking.to
    )
  end

  private

  def self.raise_error(errors)
    error_type = {
      future: PAST_TIME,
      half_hour: BAD_TIME_RANGE,
      already_booked: ALREADY_BOOKED,
      he_is_drunk: HE_IS_DRUNK
    }.fetch(errors.details.keys.first, INVALID_PARAMS)

    raise error_type, errors.messages
  end
end
