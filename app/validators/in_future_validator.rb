class InFutureValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return unless record.from

    if Time.now >= record.from
      record.errors.add attribute, 'It is not possible to book in the past'
    end
  end
end
