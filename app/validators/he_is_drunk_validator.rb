class HeIsDrunkValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    @from = record.from
    @to = record.to
    @user_id = record.user_id

    return unless @from and @to

    if reserved?
      record.errors.add attribute, 'Unfortunately, this table is already booked'
    end
  end

  private

  def reserved?
    range = @from..@to
    Reservation.where(user_id: @user_id, from: range, to: range).any?
  end
end
