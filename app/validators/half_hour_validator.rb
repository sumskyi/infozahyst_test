class HalfHourValidator < ActiveModel::EachValidator
  # minutes * seconds in 1 minute
  ALLOWED_SECONDS_RANGE = 30 * 60

  def validate_each(record, attribute, value)
    return unless record.from && record.to

    range = (record.to - record.from).to_i

    if range % ALLOWED_SECONDS_RANGE != 0
      record.errors.add attribute, 'Only 30 minutes ratio is allowed'
    end
  end
end
