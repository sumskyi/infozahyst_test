class AlreadyBookedValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    @table_id = record.table_id
    @table = nil
    return unless table

    @from = record.from
    @to = record.to

    if reserved?
      record.errors.add attribute, 'Unfortunately, this table is already booked'
    end
  end

  private

  def table
    @table ||= Table.find_by_id(@table_id)
  end

  def reserved?
    range = @from..@to
    table.reservations.where(from: range, to: range).any?
  end
end
