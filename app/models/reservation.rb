class Reservation < ApplicationRecord
  belongs_to :restaurant
  belongs_to :user
  belongs_to :table

  validates_presence_of :restaurant_id, :user_id, :table_id
end
