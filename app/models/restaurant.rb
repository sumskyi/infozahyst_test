class Restaurant < ApplicationRecord
  has_many :tables
  has_many :reservations

  with_options presence: true do
    validates :name, uniqueness: true
    validates :opening
    validates :closing
  end

  def rand_free_table(from, to)
    tables.limit(1).order('RANDOM()').
      left_joins(:reservations).
      where("(reservations.from <= '#{from}' AND reservations.to <= '#{to}') OR (reservations.from >= '#{from}' AND  reservations.to >= '#{to}' ) ").first
  end
end
