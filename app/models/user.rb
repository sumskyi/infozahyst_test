class User < ApplicationRecord
  with_options presence: true do
    validates :first_name, :last_name, :id_card
  end
end
