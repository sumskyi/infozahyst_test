class CreateTables < ActiveRecord::Migration[6.0]
  def change
    create_table :tables do |t|
      t.references :restaurant, foreign_key: true
      t.integer :number, null: false
    end

    add_index :tables, [:restaurant_id, :number], unique: true
  end
end
