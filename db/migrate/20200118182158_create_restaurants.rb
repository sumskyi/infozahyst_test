class CreateRestaurants < ActiveRecord::Migration[6.0]
  def change
    create_table :restaurants do |t|
      t.string :name, null: false, index: { unique: true }

      opts = { null: false, precision: 0 }
      t.time :opening, opts
      t.time :closing, opts
    end
  end
end
