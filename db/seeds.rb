# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

r1 = Restaurant.create(name: 'Shalena Bdzhilka', opening: '10:00', closing: '23:00')
r2 = Restaurant.create(name: 'Hendelyk', opening: '05:00', closing: '03:00')

t1 = Table.create(restaurant: r1, number: 1)
t2 = Table.create(restaurant: r1, number: 2)
t3 = Table.create(restaurant: r1, number: 3)
t4 = Table.create(restaurant: r2, number: 1)
t5 = Table.create(restaurant: r2, number: 2)
t6 = Table.create(restaurant: r2, number: 3)

u1 = User.create(first_name: 'Vlad', last_name: 'Sumskyi', id_card: '1234567')
u2 = User.create(first_name: 'Uncle', last_name: 'Vasya', id_card: '765432')

Reservation.create(restaurant: r1, user: u1, table: t1, from: '2050-01-15 15:00', to: '2050-01-15 16:00')
Reservation.create(restaurant: r1, user: u2, table: t2, from: '2050-01-16 15:00', to: '2050-01-16 16:00')
Reservation.create(restaurant: r2, user: u1, table: t3, from: '2050-01-17 15:00', to: '2050-01-17 16:00')
Reservation.create(restaurant: r2, user: u2, table: t4, from: '2050-01-18 15:00', to: '2050-01-18 16:00')

Reservation.create(restaurant: r1, user: u1, table: t1, from: '2049-01-15 15:00', to: '2049-01-15 16:00')
Reservation.create(restaurant: r1, user: u2, table: t2, from: '2049-01-16 15:00', to: '2049-01-16 16:00')
Reservation.create(restaurant: r2, user: u1, table: t3, from: '2049-01-17 15:00', to: '2049-01-17 16:00')
Reservation.create(restaurant: r2, user: u2, table: t4, from: '2049-01-18 15:00', to: '2049-01-18 16:00')
