require 'rails_helper'

RSpec.describe Table, type: :model do
  describe 'relations' do
    it { should belong_to(:restaurant) }
    it { should have_many(:reservations) }
  end
end
