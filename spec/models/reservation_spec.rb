require 'rails_helper'

RSpec.describe Reservation, type: :model do
  describe 'relations' do
    it { should belong_to(:restaurant) }
    it { should belong_to(:user) }
    it { should belong_to(:table) }
  end

  describe 'validations' do
    it { should validate_presence_of(:restaurant_id) }
    it { should validate_presence_of(:user_id) }
    it { should validate_presence_of(:table_id) }
  end
end
