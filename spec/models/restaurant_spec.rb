require 'rails_helper'

RSpec.describe Restaurant do
  describe 'validations' do
    subject { Restaurant.create(name: 'A', opening: '07:00', closing: '23:00') }

    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:opening) }
    it { should validate_presence_of(:closing) }
    it { should validate_uniqueness_of(:name) }
  end

  describe 'associations' do
    it { should have_many(:tables) }
    it { should have_many(:reservations) }
  end
end
