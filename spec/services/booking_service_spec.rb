require 'rails_helper'

RSpec.describe BookingService, type: :model do
  describe 'presence validations' do
    subject do
      BookingService.new(
        restaurant_id: 666,
        user_id: 1488,
        table_id: 123,
        from: Time.now + 1.day,
        to: Time.now + 2.days
      )
    end

    it { should validate_presence_of(:restaurant_id) }
    it { should validate_presence_of(:user_id) }
    it { should validate_presence_of(:table_id) }
    it { should validate_presence_of(:from) }
    it { should validate_presence_of(:to) }
  end


  describe 'restrictions' do
    before do
      Rails.application.load_seed
    end

    let(:restaurant) { Restaurant.first }
    let(:table) { restaurant.tables.first }
    let(:user) { User.first }

    let(:another_restaurant) { Restaurant.last }
    let(:another_table) { another_restaurant.tables.first }

    it 'cannot book in the past' do
      expect do
        BookingService.book do
          {
            restaurant_id: restaurant.id,
            user_id: user.id,
            table_id: table.id,
            from: Time.now - 1.day,
            to: Time.now + 1.day
          }
        end
      end.to raise_error(BookingService::PAST_TIME)
    end

    it 'booking in 30 minutes steps only' do
      tomorrow = Time.now + 1.day
      later = tomorrow + 95.minutes

      expect do
        BookingService.book do
          {
            restaurant_id: restaurant.id,
            user_id: user.id,
            table_id: table.id,
            from: tomorrow,
            to: later
          }
        end
      end.to raise_error(BookingService::BAD_TIME_RANGE)
    end

    it 'cannot book already booked table' do
      expect do
        BookingService.book do
          {
            restaurant_id: restaurant.id,
            user_id: user.id,
            table_id: table.id,
            from: Time.parse('2050-01-15 15:00'),
            to: Time.parse('2050-01-15 16:00')
          }
        end
      end.to raise_error(BookingService::ALREADY_BOOKED)
    end

    it 'same user cannot book many reservations at the same time ??' do
      expect do
        BookingService.book do
          {
            restaurant_id: another_restaurant.id,
            user_id: user.id,
            table_id: another_table.id,
            from: Time.parse('2050-01-15 15:00'),
            to: Time.parse('2050-01-15 16:00')
          }
        end
      end.to raise_error(BookingService::HE_IS_DRUNK)
    end
  end

  describe 'happy way' do
    before do
      Rails.application.load_seed
    end

    let(:user) { User.last }
    let(:restaurant) { Restaurant.last }
    let(:from) { Time.parse('2030-01-15 15:00') }
    let(:to) { from + 60.minutes }
    let(:table) { restaurant.rand_free_table(from, to) }

    # and finally!
    it 'creates a reservation' do
      expect do
        BookingService.book do
          {
            restaurant_id: restaurant.id,
            user_id: user.id,
            table_id: table.id,
            from: from,
            to: to
          }
        end
      end.to change{ Reservation.count }.by(1)
    end
  end

  describe 'Working hours valid options - ' do
    before do
      Rails.application.load_seed
    end

    let(:restaurant) { Restaurant.last }
    let(:user) { User.last }
    let(:table) { restaurant.rand_free_table(from, to) }
    let(:from) { Time.parse('2049-01-15 04:00') }
    let(:to) { Time.parse('2049-01-15 03:00')  }

    xit "works from 04:00 to 03:00" do
      expect do
        BookingService.book do
          {
            restaurant_id: restaurant.id,
            user_id: user.id,
            table_id: table.id,
            from: from,
            to: to
          }
        end
      end.to raise_error(BookingService::CLOSED_TIME)
    end
  end
end
