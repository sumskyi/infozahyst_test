# Infozahyst test task

Необходимо сделать систему для резервации столов в ресторанах.

В каждом ресторане есть столы. Эти столы можно резервировать.

При этом шаг резервации - 30 минут, а пользователь может резервировать стол на 30, 60, 90 и т.д. минут.

В каждой резервации должен быть пользователь. У каждого ресторана есть график работы, который может переваливать за полночь.

Резервации на один стол не должны пересекаться. При этом, если одна резервация закончилась в 5 вечера, вторая может начинаться в 5 вечера.

Интересует только реализация моделей и структура базы данных.

## How it works

* ruby 2.6.3p62
* postgresql
* edit '.env.development.local' and '.env.test.local' to set user/pass for postgres db
* run `bundle exec rails db:create`
* run `bundle exec rails db:migrate RAILS_ENV=test`
* there is no any admin area, to make sure the code is working you should use `bundle exec rspec`
* steps to make the code better is refactoring and probably changing the interface of BookingService
* also, clean up the app from extra things like webpack, jobs, channels, javascript and so on. bit, nevermind
* in short, to understand how it works, see ***spec/models***, ***spec/services***, ***app/models***, ***app/services***, and ***app/validators*** directory
* the db structure you can see in the **db/schema.rb**
* unfortunally I had not enough time to implement reservation validations related to restaurant working hours, it a bit hard to implement, if it opens for instance at 05:00 and closes at 03:00 next day, it needs more time to think

[comment]: <> (від:	Nikolay Andreev <nandreev@warfare-tec.com>)
[comment]: <> (кому:	Vlad Sumskyi <sumskyi@gmail.com>)
[comment]: <> (дата:	16 груд. 2019 р., 14:46)
[comment]: <> (тема:	Re: Тестове завдання Інфозахист)
[comment]: <> (підписано:	warfare-tec.com)

